\documentclass[a4paper,12pt]{article}
%\documentclass[a4paper,10pt]{scrartcl}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{setspace}
\onehalfspacing
%\linespread{1.3}
\usepackage{amsthm}
\usepackage{float}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{pdfpages}
\theoremstyle{definition}
%\usepackage[slovak]{babel}
\usepackage[all]{xy}
\usepackage[top=25mm, bottom=25mm, left=35mm, right=20mm]{geometry}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
%\theoremstyle{definition}
\title{Homotopy types of independence complexes of some graphs}
\author{Bc. Dávid Juhász}
\date{\today}

\pdfinfo{%
  /Title    (Clanok)
/Author   (Bc. Dávid Juhász)
  /Creator  (Bc. Dávid Juhász)
  /Producer ()
  /Subject  ()
  /Keywords (graph, matching tree)
}
\newtheorem{definition}{Definition}[section]
\theoremstyle{definition}
\newtheorem{example}{\emph{Example}}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{prof}{Proof}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{prop}{Property}[section]
%\theoremstyle{theorem}
\begin{document}
\begin{center}
 \textbf{HOMOTOPY TYPES OF INDEPENDENCE COMPLEXES OF SOME GRAPHS}
\end{center}
\begin{center}
 DÁVID JUHÁSZ
\end{center}

\abstract
In this report we determine the homotopy type of independence complexes of certain families of graphs using the tools from 
Forman's discrete Morse theory. Also, we determine the invariants of a graph $G$ due to its topological properties of a complex of the form 
$Ind(F(G))$, where $F$ is a suitably chosen endofunctor of graph category.
%\tableofcontents
\noindent
\setcounter{page}{1}
\section{Introduction}
A \emph{simplicial complex} is a geometric object constructed by "gluing" together points, line segments, triangles and their $n$-dimensional 
versions (simplices). The members of a simplicial complex are called \emph{faces}. These simplices are glued together obeying certain rules. 
If $\Delta$ is a simplicial complex and $\sigma\in\Delta$, then $\forall\tau\subseteq\sigma\Rightarrow\tau\in\Delta$ and the intersection 
of any two simplices in $\Delta$, is a face of $\Delta$.

An \emph{abstract simplicial complex} is a purely combinatorial representation of a simplicial complex. If $\tau$ is a simplex, then its dimension 
is $dim(\tau)=|\tau|-1$. We consider an empty set as a set with 0 elements and its dimension is $dim(\emptyset)=-1$, a point as a set with 1 
element, its dimension is 0, so an $n$-dimensional simplex has $n+1$ elements. The dimension of a simplicial complex $\Delta$ is 
$dim(\Delta)=max_{\sigma\subseteq\Delta}dim(\sigma)$.

\section{Preliminaries}
An independent set in a graph $G$ is a set of pairwise non-adjacent vertices. The poset of all independent set ordered by inclusion is an abstract 
simplicial complex.
\begin{definition}\cite{b}
Let $G=(V,E)$ be a graph. The \emph{independence complex} of $G$, $Ind(G)$, is the simplicial complex with vertex set $V$ and faces given by 
independent sets of $G$.
\end{definition}
 
A poset $P$ is a directed graph if we consider it as a Hasse diagram of $P$ and its edges as arrows pointing from larger to smaller 
elements.

A set $M$ of pairwise disjoint edges of $P$ is a \emph{matching} of $P$. It is said to be \emph{perfect}, if it covers all elements of $P$. The 
matching $M$ is called \emph{Morse}, if the directed graph obtained from $P$ be reversing the direction of the edges in $M$ is acyclic.\cite{b}
\begin{theorem}\cite{b}
 Let $\Delta$ be a polyhedral cell complex and let $M$ be an acyclic partial matching on the face poset of $\Delta$. Let $c_i$ denote the number 
of critical $i$-dimensional cells of $\Delta$. The space $\Delta$ is homotopy equivalent to a cell complex $\Delta_c$ with $c_i$ cells of dimension 
$i$ for each $i\geq0$, plus a single 0-dimensional cell in the case, where the empty set is paired in the matching.
\end{theorem}
If a partial matching $M$ has all the critical cells of the same dimension $i$, $\Delta$ is homotopy equivalent to a wedge of $i$-dimensional 
spheres. For a proof see \cite{b} Theorem 6.3.
\begin{theorem}\cite{b}\label{patchwork}
 If $\phi:P\rightarrow Q$ is an order-preserving map and, for each $q\in Q$, each subposet $\phi^{-1}(q)$ carries an acyclic partial matching $M_q$, 
then the union of the $M_q$ is an acyclic partial matching on $P$.
\end{theorem}

\subsection{Matching tree}\label{match}
A \emph{matching tree} is a tool used in \cite{a} to construct a Morse matching of simplices on the independence complex 
$\Sigma=Ind(G)$ of a graph $G$. The nodes of the tree are of the form
\begin{displaymath}
 \Sigma(A,B)=\{I\in\Sigma:A\subseteq I\quad and\quad B\cap I=\emptyset\},
\end{displaymath}
where
\begin{displaymath}
 A\cap B=\emptyset\qquad and\qquad N(A):=\bigcup_{a\in A}N(a)\subseteq B
\end{displaymath}
The root of the tree is $\Sigma(\emptyset,\emptyset)=\Sigma$, which is the set of all independent sets of $G$. $\Sigma(A,B)$ is also a set of 
unmatched elements. The matching tree is constructed in the following way. Choose a vertex (the \emph{tentative pivot}) $p\in V'(G)=V(G)\setminus 
(A\cup B)$ and continue according to these rules:
\begin{enumerate}
\item[$\bullet$] if $p$ has one neighbour $v$ in $V'(G)$, define the set $\Delta(A,B,p)$ as a subset of $\Sigma(A,B)$:
\begin{displaymath}
 \Delta(A,B,p)=\{I\in\Sigma:A\subseteq I\quad and\quad B\cap I=I\cap N(p)=\emptyset\}.
\end{displaymath}
and do all the matchings $(I,I\cup \{p\})$, for $I\in\Delta(A,B,p)$. Denote the set of these matchings $M(A,B,p)$. Then give to the node 
$\Sigma(A,B)$ one child, specifically $\Sigma(A\cup\{v\},B\cup N(v))$ and name the new edge of the tree by the pivot $p$. This set is empty, 
if $p$ has no neighbour. In this case, we call $p$ a free vertex of $\Sigma(A,B)$, so we reached a complete matching. The 3-tuple $(A,B,p)$ is 
a \emph{matching site} of the tree.
\item[$\bullet$] if $p$ has more than one neighbour, choose one neighbour $v$ in $V'(G)$. The node $\Sigma(A,B)$ has 2 children. The left one is 
$\Sigma(A,B\cup\{v\})$ and right one $\Sigma(A\cup\{v\},B\cup N(v))$. Index these two new edges by the \emph{splitting vertex} $v$. We call 
$(A,B,v)$ a \emph{splitting site} of the tree.
\end{enumerate}
If the node is the empty set, there are no unmatched elements, we call it a leaf. At the end, we obtain a matching $M$ of $\Sigma$ taking the union 
of all partial matchings $M(A,B,p)$ at the matching sites of the tree. See Theorem \ref{patchwork}. The unmatched elements are those at the leaves. 
On Figure \ref{fig1} we can see an example of a matching tree. The tentative pivot is marked with * and the splitting vertex with a 
triangle. The vertices of $A$ are in black and the vertices of $B$ in white.

\begin{figure}[h]\caption{Example of a matching tree.\cite{a}}\label{fig1}
\mbox{}\\
 \includegraphics{obr13}
\end{figure}

The matching tree on $G$ yields an acyclic partial matching on the face poset of $Ind(G)$.
\begin{theorem}\cite{a}
 A matching tree $M(G)$ for $G$ yields an acyclic partial matching on the face poset of $Ind(G)$ whose critical cells are given by the non-empty 
sets $\Sigma(A,B)$ labeling non-root leaves of $M(G)$. In particular, for such a set $\Sigma(A,B)$, the set $A$ yields a critical cell in 
$Ind(G)$.
\end{theorem}

\section{Using independence complex to determine properties of graphs}\label{theorem}
Let $J$ be an operation on graphs defined as follows: take a graph $G$ and make a copy of it $G'$. Join all the pairs of vertices 
$(v_i\in G, v'_i\in G')$ with an edge then subdivide the new edges. The subdividing vertices are $P=\{p_i\}_{i=1}^{|V(G)|}$.

\begin{theorem}\label{J(G)}
 Suppose, $G$ is a graph, that consists of $b$ isolated bipartite parts, $n$ isolated non-bipartite parts and $s$ isolated vertices. Then
\begin{itemize}
 \item $Ind(J(G))\cong point$ if $b=0,s=0$ and $n\neq 0$
 \item $Ind(J(G))\cong \bigvee_{2^b} S^{|V(G\setminus G_n)|-1}$ otherwise,
\end{itemize}
where $G_n$ is a subgraph of $G$ containing only the non-bipartite parts.
\end{theorem}
\noindent
\textsl{Proof. }Let us start with a case, when $s=0,n=0$ and $b\neq 0$. By constructing a matching tree, the pivots are picked from $P$. Fix one 
2-coloring $\gamma$ on $G$ and on $G'$ a reversed one $\gamma'$. At each matching or splitting site we create a subgraph of a spanning 
tree(subtree) $S$ for $G$ and $S'$ for $G'$. The notation of the leaves on the matching tree is changed to $\Sigma(A,B,S,S')$. The vertices of $S$ 
and $S'$ are 
\begin{itemize}
 \item $V(S)\subseteq V(G)\cap(A\cup B)$
 \item $V(S')\subseteq V(G')\cap(A\cup B)$
\end{itemize}
at the site $\Sigma(A,B,S,S')$. We assign to $V(S)$ the same colors as in $\gamma$ and to $V(S')$ as in $\gamma'$. The beginning of the matching 
tree will be always in this way: let the first pivot be $p_1$ and the splitting vertex $v_1$. Then the left side is 
$\Sigma(\emptyset,\emptyset)\xrightarrow{p_1}\Sigma(\emptyset,\{v_1\},S,S')$, where the spanning subtree $S$ is one single vertex $v_1$ and 
$S'=\emptyset$. The right side is $\Sigma(\emptyset,\emptyset)\xrightarrow{p_1}\Sigma(\{v_1\},N(v_1),S,S')$, where $S$ is vertex $v_1$ joining all 
the vertices from $N(v_1)\backslash p_1$ and $S'=\emptyset$.

The vertices on the leaves of the spanning subtrees $S,S'$ will indicate the choice of the pivots in the following way. If the vertices on the 
leaves in $S$ are $\{v_i,\ldots,v_j\}$ and $\{v'_i,\ldots,v'_j\}$ on $S'$, we choose the next pivot $p_k$,where $k\in\{i,\ldots,j\}$ and 
$p_k\notin B$. The next pivot at the left side is now $p_1$, because the vertex on the leaf of $S$ has index 1($v_1$) and on the right side the 
next pivot is $p_i$, where the index $i$ belongs to $v_i\in N(v_1)$. 

Before we proceed to the next phase of our proof, let us introduce a notation with *. Denote $G^*$ a graph $G$ or $G'$, then
\begin{itemize}
\item $G'^*=G'$, if $G^*=G$
\item $G'^*=G$, if $G^*=G'$,
\end{itemize}
%Denote $v_i^*$ a vertex from $V(G\cup G')$, then
%\begin{itemize}
%\item $v_i'^*=v'_i$, if $v_i^*=v_i$ for $v_i\in V(G)$
%\item $v_i'^*=v_i$, if $v_i^*=v'_i$ for $v'_i\in V(G')$.
%\end{itemize}
and denote the vertices of $G^*$ by $v_i^*$ and the vertices of $G'^*$ by $v_i'^*$. One could also write this notation as: let $G^*\in\{G,G'\}$, 
then $G'^*=\{G,G'\}\backslash G^*$. We introduce the same notation for the spanning subtrees $S,S'$. Let $S^*\in\{S,S'\}$, then 
$S'^*=\{S,S'\}\backslash S^*$.

Suppose, that there exists a vertex $v_i^*$ at the matching site \\$\Sigma(A,B,S^*,S'^*)$, such that 
$v_i^*\in A\cup B$ and $v_i'^*\notin A\cup B$. So there exists a pivot $p_i$, which has one free neighbour $v_i'^*$. Picking $p_i$ as a pivot, 
the next matching site is $\Sigma(A\cup v_i'^*, B\cup N(v_i'^*),S^*,S'^*\cup N(v_i'^*))$. We assign to the vertices $V(A)$ and $V(B)$ the 
same color as they have on $S^*$ or $S'^*$. 
In $N(v_i'^*)$ exists a vertex $v_j'^*\notin A\cup B$, because if not, $v_j^*\in A\cup B$, so $v_j^*$ would have a coloring. Now, we 
investigate, what happens, if $v_j^*$ has a coloring: 
\begin{itemize}
 \item $v_j^*$ can not be in $B$, because $v_j'^*$ is also in $B$ and there has to be a reversed 2-coloring on $S^*$ and $S'^*$.
\item $v_j^*$ can not be in $A$ either, because $v_j'^*\in N(v_i'^*)$ in $S'^*$ $\Rightarrow v_j^*\in N(v_i^*)$ in $S^*$ and $v_i^*$ is a leaf, so 
it can not be a parent, but $v_j^*$ can be a parent of $v_i^*$. In this case, $|N(v_i'^*)|\leq1$, since a node can not have more parents than 1. So 
if $v_j^*$ is a parent of $v_i^*$, and $v_i^*\in B$, $v_j^*$ has to be in $A$ and has a parent $v_k^*\in B$, such that $v_k^*\neq v_i^*$. But on 
$S'^*$ the parent of $v_j'^*$ is $v_i'^*$ and we deal with a contradiction.
\end{itemize}
We showed, that there always exists a vertex with one neighbour sitting on the leaves, which we can choose as a pivot. One vertex is added to $A$ 
every time we choose a pivot, so $|A|=|V(G)|=|P|$ in the end and we have 2 leaves, because there was one splitting site at the start.
 
We proceed to the case, when $G$ contains only non-bipartite parts ($G_n$). A non-bipartite graph contains a cycle subgraph of odd length $C_k$. 
Suppose, we start to construct the matching tree with choosing the pivots $p_i$, such that $v_i(v'_i)\in C_k(C'_k)$. Let us choose the first pivot 
$p_1$ and the splitting vertex $v_1$. We have a splitting site, where on the left side as the next pivot we choose the same one, $p_1$, and the 
matching site is $\Sigma(\{v'_1\},N(v'_1)\cup v_1)$. $N(v'_1)$ is non-empty, because $v'_1\in C'_k$, so there exists a pivot $p_i$, that has one 
neighbour just like each next pivot we choose, except one exception we are going to mention. Also, on the right side, there is a pivot with one 
neighbour, because $N(v_1)\neq\emptyset,v_1\in C_k$.

First, let us investigate the left side of the matching tree. At each matching site we take 2 vertices from $C_k$ or $C'_k$. One of them we add to 
$A$ and the other to $B$. At the start on $C_k$ we have $k-1$ free vertices and that is an even number what makes at the end all the vertices from 
$C_k$ taken, but 2 neighbouring will be in $B$. What happens next, Figure \ref{fig12} shows us. A free vertex will appear.

Now, let us look at the right side of the matching tree. The splitting site has the form $\Sigma(\{v_1\},N(v_i))$ and on $C_k$ there are $k-3$ 
free vertices, because $v_1$ has 2 neighbours on $C_k$, so similarly, as in the previous case, it produces a free vertex and hence, $Ind(J(G_n))$ is 
contractible. 

The case $b=0,n=0,s\neq 0$ is simple. Each isolated vertex produces after the operation $J$ a path graph with 3 vertices. The matching tree of this 
graph leaves one vertex in the set $A$.

We proceed to the general case, when $b\neq 0,n\neq0,s\neq0$. If we construct a matching tree on the first bipartite part, it gives us 2 leaves with 
critical simplices. On each leaf, we go to the next bipartite part and the matching tree of that part produces other 2 leaves with critical 
simplices. It is $2^2$ together. Then the next one produces other 2 on every previous one. At the very end, we will have $2^b$ leaves with critical 
simplices. The matching trees of the isolated vertices will not affect the this number, because it does not split. And similarly, the matching 
trees of the non-bipartite parts will not affect it either, because the leaves present "dead ends".

The dimension of those critical simplices will be the following. The matching tree of every bipartite part $G_{b_i}$ gives us critical simplices of 
dimension $V(G_{b_i})$ and every isolated vertex means one vertex in $A$. We know, that only the matching tree of any isolated non-bipartite part 
does not produce any critical simplices, that is why the dimension of all critical simplices will be $|V(G\setminus G_n)|$. $\Box$

\begin{figure}[H]\caption{Section of a matching tree of $J(G)$.}\label{fig12}
\includegraphics{obr14}
\end{figure}

Consider another modification of a graph $T(G)$. Insert 2 new vertices into every edge and create a triangle out of these vertices and one 
added isolated vertex $p_i\in P=\{p_1,p_2,\ldots,p_{|E(G)|}\}$. For example see Figure \ref{fig14}. 

\begin{figure}\caption{Example of $T(G)$.}\label{fig14}
\mbox{}\\
\centering{\includegraphics{obr15}}
\end{figure}

We are also going to introduce a few definitions. An \emph{orientation} of an undirected graph is an assignment of a direction to each edge, making 
a directed graph from the initial one. For a vertex, the number of head endpoints adjacent to a node is called the \emph{indegree} of the vertex, 
denoted by $deg^-(v)$ and the number of tail endpoints adjacent to a vertex is its \emph{outdegree}, $deg^+(v)$. A vertex with $deg^-(v)=0$ is 
called a \emph{source}, and similarly, a vertex with $deg^+(v)=0$ is called a sink.

\begin{figure}[h]\caption{Section of a matching tree of $T(G)$ for some $G$.}\label{fig15}
\mbox{}\\
\centering{\includegraphics{obr16}}
\end{figure}

\mbox{}
\begin{theorem}
 Let $G$ be a graph. Then $Ind(T(G))$ is contractible, if $G$ is a tree, else it is homotopy equivalent to $\bigvee_xS^{|E(G)|-1}$, where $x$ is 
the number of all such orientations on the edges of $G$, such that no vertex is a source.
\end{theorem}
\noindent
\textsl{Proof. }If the pivots for the matching tree will be each one of $P$, there are 2 options for each edge.These 2 options are showed on 
Figure \ref{fig15}. We labeled the vertices of an edge by $v_1,v_2,v_3$ and $v_4$ and put a direction by an arrow to the edge on left side 
pointing from $v_1$ to $v_4$ and to the edge on the right side a reversed arrow. Every time we choose a new pivot, one vertex will be added to the 
set $A$, so at the end, all the critical simplices will have $|E(G)|$ vertices, because $|P|=|E(G)|$. On each edge we have both directions 
appearing in the matching tree, among which only one combination produces a free vertex. It happens, when $deg^-(v)=0$. This means a situation, when 
on each edge, that have one vertex in common, the arrow is pointing out from the vertex. 

A tree graph can not have an orientation on the edges without a vertex being a source and hence, it is contractible. $\Box$

We showed, that the homotopy type of this modification $Ind(J(G))$ depends on the bipartitness of the graph and the number of spheres 
at the second modification $Ind(T(G))$ is number of source-free orientations of the graph.

\newpage
\section{Bibliography}
\begin{thebibliography}{99}
\bibitem{a} MIREILLE BOUSQUET-MÉLOU, SVANTE LINUSSON, ERAN NEVO: \emph{On the independence complex of square grids}, 
J. Algebraic Combin. 27 (2008), 423–450.
\bibitem{b} ROBIN FORMAN: \emph{A user's guide to discrete Morse theory}, \textsl{Sem. Lothar. Combin.}, B48c:1–35, 2002.
\bibitem{c} DMITRY KOZLOV: \emph{Combinatorial Algebraic Topology}, Springer, Berlin and Heidelberg, 2008, ISBN 978-3-540-71961-8.
\bibitem{e} D.N. KOZLOV, Complexes of directed trees, J. Comb. Theory Ser. A 88 (1999), pp. 112–122.
\end{thebibliography}
\newpage
\pagenumbering{gobble}
\end{document}
